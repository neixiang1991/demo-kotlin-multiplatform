package com.example.demo

import androidx.compose.runtime.Composable
import androidx.compose.ui.platform.LocalConfiguration
import androidx.compose.ui.unit.Dp
import androidx.compose.ui.unit.TextUnit
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp

/**
 * @Author: milin
 * @Create: 2024/3/28 16:12
 * @Desc  :
 */

@Composable
fun scale(p: Float): Float {
    return p * LocalConfiguration.current.screenWidthDp / 750
}

val Int.dp: Dp
    @Composable
    get() = scale(toFloat()).dp

val Int.sp: TextUnit
    @Composable
    get() = scale(toFloat()).sp
