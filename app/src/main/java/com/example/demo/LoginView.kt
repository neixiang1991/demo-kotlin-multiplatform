package com.example.demo

import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.border
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.RowScope
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.offset
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.layout.width
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.foundation.text.BasicTextField
import androidx.compose.material3.AlertDialog
import androidx.compose.material3.Divider
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.MutableState
import androidx.compose.runtime.getValue
import androidx.compose.runtime.setValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.text.TextStyle
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.input.PasswordVisualTransformation
import androidx.compose.ui.text.input.VisualTransformation
import androidx.compose.ui.tooling.preview.Preview
import io.ktor.client.HttpClient
import io.ktor.client.plugins.contentnegotiation.ContentNegotiation
import io.ktor.client.request.post
import io.ktor.client.request.setBody
import io.ktor.client.statement.bodyAsText
import io.ktor.http.ContentType
import io.ktor.http.contentType
import io.ktor.serialization.kotlinx.json.json
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

/**
 * @Author: milin
 * @Create: 2024/3/28 15:59
 * @Desc  :
 */

@Composable
fun LoginView() {
    val username = remember { mutableStateOf("") }
    val password = remember { mutableStateOf("") }
    var eyeOpen by remember { mutableStateOf(false) }
    val buttonDisabled = username.value.isEmpty() || password.value.length < 6
    var loginResult by remember { mutableStateOf("") }

    fun onLoginClicked() {
        if (buttonDisabled) return

        CoroutineScope(Dispatchers.IO).launch {
            try {
                val client = HttpClient { install(ContentNegotiation) { json() } }
                loginResult = client.post("https://mock.apifox.com/m1/4102536-0-default/demo/login") {
                    contentType(ContentType.Application.Json)
                    setBody(mapOf(Pair("username", username.value), Pair("password", password.value)))
                }.bodyAsText()
            } catch (e: Exception) {
                loginResult = e.message ?: "登录失败"
            }
        }
    }

    Box(
        Modifier
            .fillMaxSize()
            .background(Color.White)
    ) {
        Image(
            painter = painterResource(id = R.drawable.icon_back),
            contentDescription = null,
            modifier = Modifier
                .width(18.dp)
                .height(30.dp)
                .offset(32.dp, 38.dp),
        )
        Text(
            text = "预约开通",
            modifier = Modifier
                .align(Alignment.TopEnd)
                .offset((-32).dp, 40.dp),
            color = Color(0xFF666666),
            fontSize = 26.sp,
            fontWeight = FontWeight.W600,
        )

        Column(Modifier.fillMaxSize()) {
            Image(
                painter = painterResource(id = R.drawable.logo_sqb),
                contentDescription = null,
                modifier = Modifier
                    .align(Alignment.CenterHorizontally)
                    .padding(top = 219.dp, bottom = 90.dp)
                    .width(300.dp)
                    .height(95.dp)
            )

            TextInput(
                text = username,
                placeholder = "请输入手机号/邮箱",
            )
            TextInput(
                text = password,
                placeholder = "请输入登录密码",
                visualTransformation = if (eyeOpen) VisualTransformation.None else PasswordVisualTransformation()
            ) {
                Image(
                    painter = painterResource(id = if (eyeOpen) R.drawable.icon_eye_1 else R.drawable.icon_eye_0),
                    contentDescription = null,
                    modifier = Modifier
                        .padding(start = 12.dp)
                        .size(30.dp)
                        .clickable { eyeOpen = !eyeOpen }
                )
                Divider(
                    Modifier
                        .padding(horizontal = 22.dp)
                        .height(26.dp)
                        .width(2.dp),
                    color = Color(0xFFE5E5E5)
                )
                Text(text = "忘记密码", fontSize = 26.sp, color = Color(0xFF666666))
            }

            Box(
                Modifier
                    .padding(top = 63.dp, start = 64.dp, end = 64.dp)
                    .fillMaxWidth()
                    .height(88.dp)
                    .clip(RoundedCornerShape(44.dp))
                    .background(Color(if (buttonDisabled) 0xFFf5e2b8 else 0xFFEE9E00))
                    .clickable(enabled = !buttonDisabled) { onLoginClicked() }
            ) {
                Text(
                    text = "登录",
                    modifier = Modifier.align(Alignment.Center),
                    color = Color.White,
                    fontSize = 32.sp,
                )
            }
            Text(
                text = "手机验证码登录",
                modifier = Modifier.padding(top = 32.dp, start = 64.dp, end = 64.dp),
                fontSize = 26.sp,
                color = Color(0xFF666666),
            )

            Spacer(modifier = Modifier.weight(1f))

            Row(Modifier.padding(bottom = 188.dp, start = 64.dp, end = 64.dp)) {
                Third(image = R.drawable.icon_wechat, text = "微信登录")
                Third(image = R.drawable.icon_alipay, text = "支付宝登录")
            }
        }
    }

    if (loginResult.isNotEmpty()) {
        AlertDialog(
            onDismissRequest = { },
            title = { Text(text = loginResult, fontSize = 32.sp) },
            confirmButton = {
                Text(
                    text = "确定",
                    modifier = Modifier.clickable { loginResult = "" },
                    fontSize = 32.sp,
                )
            }
        )
    }
}

@Preview(showBackground = true)
@Composable
fun LoginViewPreview() {
    LoginView()
}

@Composable
fun TextInput(
    text: MutableState<String>,
    placeholder: String,
    visualTransformation: VisualTransformation = VisualTransformation.None,
    content: @Composable () -> Unit = @Composable {},
) {
    Row(
        Modifier
            .padding(top = 30.dp, start = 64.dp, end = 64.dp)
            .fillMaxWidth()
            .height(99.dp),
        verticalAlignment = Alignment.CenterVertically
    ) {
        BasicTextField(value = text.value,
            onValueChange = { text.value = it },
            modifier = Modifier.weight(1f),
            textStyle = TextStyle(color = Color.Black, fontSize = 36.sp),
            visualTransformation = visualTransformation,
            decorationBox = { innerTextField ->
                if (text.value.isEmpty()) {
                    Text(
                        text = placeholder,
                        modifier = Modifier.padding(top = 6.dp),
                        color = Color(0xFF999999),
                        fontSize = 28.sp
                    )
                }
                innerTextField()
            })
        Image(painter = painterResource(id = R.drawable.icon_close),
            contentDescription = null,
            modifier = Modifier
                .size(30.dp)
                .clickable { text.value = "" })
        content()
    }
    Divider(
        Modifier
            .padding(horizontal = 64.dp)
            .height(1.dp)
            .background(Color(0xFFE5E5E5))
    )
}

@Composable
fun RowScope.Third(image: Int, text: String) {
    Column(modifier = Modifier.weight(1f), horizontalAlignment = Alignment.CenterHorizontally) {
        Image(
            painter = painterResource(id = image),
            contentDescription = null,
            modifier = Modifier
                .width(90.dp)
                .height(90.dp)
                .clip(RoundedCornerShape(45.dp))
                .border(1.dp, Color(0xFFE5E5E5), RoundedCornerShape(45.dp)),
        )
        Text(
            text = text,
            modifier = Modifier.padding(top = 12.dp),
            color = Color(0xFF999999),
            fontSize = 22.sp
        )
    }
}
