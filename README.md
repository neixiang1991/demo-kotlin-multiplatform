### 步骤

1. 用Android Studio创建项目，选择 Empty Compose Activity

2. 账号密码

```
10086
111111
```

### 注意

- dp和sp适配屏幕

```
见 Extensions.kt
```

- 使用三方库

```
// 插件
kotlin("plugin.serialization") version "1.9.22"
// 网络请求
val ktor_version = "2.3.9"
implementation("io.ktor:ktor-client-core:$ktor_version")
implementation("io.ktor:ktor-client-android:$ktor_version")
implementation("io.ktor:ktor-client-content-negotiation:$ktor_version")
implementation("io.ktor:ktor-serialization-kotlinx-json:$ktor_version")
```
